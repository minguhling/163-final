﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class Bloom : MonoBehaviour
{
    [Range(1, 64)]
    public int strength = 1;

    [Tooltip("Pixel threshold for bloom, which pixels contribute to bloom effect")]
    [Range(0, 10)]
    public float threshold = 1;

    public Shader bloomShader;
    [System.NonSerialized]
    Material bloom;

    const int PrefilterPass = 0;
    const int BoxDownPass = 1;
    const int BoxUpPass = 2;
    const int ApplyBloomPass = 3;

    //Rendering function to create bloom effect
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (bloom == null)
        {
            bloom = new Material(bloomShader);
            bloom.hideFlags = HideFlags.HideAndDontSave;
        }
        bloom.SetFloat("_Threshold", threshold);

        RenderTexture[] textures = new RenderTexture[16];

        //Taking half the source dimensions for bilinear sampling
        int w = source.width / 2;
        int h = source.height / 2;
        RenderTextureFormat f = source.format;

        RenderTexture currentRT = RenderTexture.GetTemporary(w, h, 0, f);
        textures[0] = currentRT;
        //intermediate blitting between source and dest
        Graphics.Blit(source, currentRT, bloom, PrefilterPass);
        RenderTexture currentSource = currentRT;

        int i = 1;
        //loop for each step set by the strength var
        for (; i < strength; i++)
        {
            w /= 2;
            h /= 2;
            //bounds checking 
            if (h < 2)
            {
                break;
            }

            currentRT = RenderTexture.GetTemporary(w, h, 0, f);
            textures[i] = currentRT;
            Graphics.Blit(currentSource, currentRT, bloom, BoxDownPass);
            //RenderTexture.ReleaseTemporary(currentSource);
            currentSource = currentRT;
        }
        //Second loop for upsampling
        for (i -= 2; i >= 0; i--)
        {
            currentRT = textures[i];
            textures[i] = null;
            Graphics.Blit(currentSource, currentRT, bloom, BoxUpPass);
            RenderTexture.ReleaseTemporary(currentSource);
            currentSource = currentRT;
        }

        //Blur final destination after looping
        bloom.SetTexture("_SourceTex", source);
        Graphics.Blit(currentSource, destination, bloom, ApplyBloomPass);
        //Clean up temp texture
        RenderTexture.ReleaseTemporary(currentSource);
    }
}
