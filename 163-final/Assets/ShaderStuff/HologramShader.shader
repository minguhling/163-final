﻿Shader "Unlit/HologramShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _HoloTex ("Hologram Sample", 2D) = "white" {}
        _LineDistance("Line Distance", float) = .1
        _Tint("Color Tint", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float4 o_vertex : TEXCOORD1;
            };

            sampler2D _MainTex;
            sampler2D _HoloTex;
            float4 _Tint;
            float4 _MainTex_ST;
            float _LineDistance;

            v2f vert (appdata v)
            {
                v2f o;
                o.o_vertex = v.vertex;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col      = tex2D(_MainTex, i.uv);
                i.uv.y -= .1 * _Time;
                fixed4 hologram = tex2D(_HoloTex, i.uv);

                if(col.r == 0 && col.g == 0 && col.b == 0 ){
                    clip(-1);
                }

                if(hologram.r == 0){
                    clip(-1);
                }

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col * _Tint;
            }
            ENDCG
        }
    }
}
