﻿Shader "Unlit/HologramShaderThree"
{
    Properties
    {
        _Gradient ("Gradient", 2D) = "white" {}
        _Tint("Color Tint", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        LOD 100
        // HANDLING ACTUAL HOLONESS FOR LIGHTBEAM
        Pass
        {
            Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
            ZWrite Off
            Cull Back
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 o_vertex : TEXCOORD1;
                float2 uv : TEXCOORD0;
            };

            sampler2D _Gradient;
            float4 _Gradient_ST;
            float4 _Tint;


            v2f vert (appdata v)
            {
                v2f o;
                o.o_vertex = v.vertex;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _Gradient);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_Gradient, i.uv);
                fixed4 endColor = _Tint;
                endColor.a = col.r;
                return endColor;
            }
            ENDCG
        }
    }
}