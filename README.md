# Graphics-Final Project | Cyberpunk Scene

![Scene Screenshot](Cyberpunk.png)
## Video link: 
https://www.youtube.com/watch?v=Dq3UmnB4mCw

## Running The Project:
This is a Windows build of a Unity project that plays upon opening the executable. Once running, you can navigate about the scene using the WASD buttons on the keyboard and change the camera angle by right-clicking and moving the mouse. 

## Windows Executable
https://drive.google.com/file/d/1qAIq0T_FqdSPf0HHg7ktn53cowa27FZy/view?usp=sharing

## Contributors:
### Michael Ettinger:
Worked on creating scrolling holographic shader for use with animated models and static planes.

### Briant Licup:
Modeled 3D space and worked on lighting setups, audio integration, and surface shaders.

### Nicolas Ming
Handled creation of particle system which produced rainfall, a rippling effect on collision, and a smoke texture to simulate splashing. 

### Nichole Boothroyd
Implemented bloom shader for recreating a neon light effect. 


